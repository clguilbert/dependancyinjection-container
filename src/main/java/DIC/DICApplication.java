package main.java.DIC;

import main.java.DIC.Services.Garage.*;
import main.java.DIC.Services.GraphResolution.A;
import main.java.DIC.Services.Injections.*;
import main.java.DIC.Services.Movies.AuditService;
import main.java.DIC.Services.Movies.MovieFinder;
import main.java.DIC.Services.Movies.MovieLister;
import main.java.DIC.Services.Movies.WebMovieFinder;

import java.util.List;

public class  DICApplication {

	public static void main(String[] args) throws Exception {
		DIContext context = DIContext.createContext();
		doBusinessLogic(context);
		movieTime(context);
		graphResolution(context);
		pimpMyClass(context);
	}

	private static void graphResolution(DIContext context) {
		A a = context.getServiceInstance(A.class);
		System.out.println(a.bonjour());
	}

	private static void doBusinessLogic(DIContext context){
		ServiceA serviceA = context.getServiceInstance(ServiceA.class);
		ServiceB serviceB = context.getServiceInstance(ServiceB.class);
		ServiceMaster serviceM = context.getServiceInstance(ServiceMasterImpl.class);
		ServiceAImpl serviceAImpl = context.getServiceInstance(ServiceAImpl.class);

		System.out.println(serviceA.jobA());
		System.out.println(serviceB.jobB());
		System.out.println(serviceAImpl.jobA());
		System.out.println(serviceM.jobPrint());
		System.out.println("Test de binding value : " + serviceM.getTextInjecte());
	}

	public static void pimpMyClass(DIContext context)
	{
		Bolide b = context.getServiceInstance(Bolide.class);
		Hybride h = context.getServiceInstance(Hybride.class);
		Voiture v = context.getServiceInstance(Voiture.class);

		System.out.println(b.sortirDuGarage());
		System.out.println(h.sortirDuGarage());
		System.out.println(v.sortirDuGarage());
	}

	private static void movieTime(DIContext context)
	{
		try {
			context.register(AuditService.class);
			context.register(MovieFinder.class, WebMovieFinder.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		MovieLister movieLister = context.getServiceInstance(MovieLister.class);
		movieLister.AddMovie("La soupe aux Choux");
		List<String> films = movieLister.getMovies();
	}
}
