package main.java.DIC;

import java.util.*;

/*
    Classe détaillant un registre pour les injections réalisées grâce à l'annotation @Valeur
 */
public class BindingValue {
    /*
        HashMap associant une ou plusieurs clés à une valeur
     */
    private Map<String , Object> valueRegistery;

    /*
        Methode servant à retourner un objet à partir d'une clé fournie par @Valeur
     */
    public <T> T getValue(String key)
    {
        return (T) valueRegistery.get(key);
    }

    /*
        Constructeur de Binding Value, initialisant la hashMap et remplissant les valeurs nécessaires
     */
    public BindingValue()
    {
        valueRegistery  = new HashMap<>();
        valueRegistery.put("test","oups");
        valueRegistery.put("test2","oups");
        valueRegistery.put("oups","bonjour");
        valueRegistery.put("int",42);
    }

    /*
        Méthode servant à ajouter une clé et une valeur associée
     */
    public void addKeyValue(String k, Object v)
    {
        valueRegistery.put(k,v);
    }
}
