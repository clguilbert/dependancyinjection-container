package main.java.DIC;

import main.java.DIC.Annotations.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/*
    Classe représentant le conteneur d'injection de dépendances
 */
public class DIContext {

    //Liste d'objets représentant les instances des services pouvant être injectés
    private final Set<Object> serviceInstances = new HashSet<>();

    //Liste de constructeurs des objets possédant des dépendances à injecter par constructeurs, et devant être construits
    //après alimentation de la liste d'instances
    private final Set<Constructor<?>> objectsToBuild = new HashSet<>();

    //Map des objets associant classes abstraites à instances concrètes
    private final Map<Class<?>,Class<?>> abstractRegisty = new HashMap<>();

    //Objet contenant le registre des valeurs pouvant être injectées par Binding Value
    private BindingValue values = new BindingValue();


    /*
        Methode servant à retourner un contexte nouvellement créé en préenregistrant les classes trouvées par le ClassScanner
        Ici, on cherche à ajouter toutes les classes du package main.java.DIC étant annotées Scannable
     */
    public static DIContext createContext() throws Exception {
        try{
            return new DIContext(DIClassScanner.getInstance("main.java.DIC")
                    .findAllClassHavingAnnotation(Scannable.class));
        }
        catch (Exception e)
        {
            throw new Exception("Erreur lors de la création du contexte");
        }

    }

    /*
        Constructeur de Contexte du container d'injection de dépendances créé à partir d'une liste de classes
     */
    public <T> DIContext(Collection<Class<T>> serviceClasses) throws Exception {
        boolean buildLater = false;
        //values.addKeyValue("rien","J'suis plus null!");

        //Pour chacune des classes de la collection fournie
        for(Class<?> serviceClass : serviceClasses){
            //Récupère la liste des constructeurs de la classe
            Constructor<?>[] constructors = serviceClass.getConstructors();
            for(Constructor<?> c : constructors)
            {
                //Verifie s'il y a besoin d'une injection par constructeur
                if(c.isAnnotationPresent(AutocableConstructor.class))
                {
                    //Si oui, le constructor est ajouté à la liste des instances à construire plus tard
                    this.objectsToBuild.add(c);
                    buildLater = true;
                }
            }
            if(!buildLater)
            {
                addInstance(serviceClass);
            }
            buildLater = false;
        }
        //Pour chaque objet instancié
        for(Object serviceInstance : this.serviceInstances){
            for(Method method : serviceInstance.getClass().getMethods())
            {
                //On vérifie si une méthode est un setter chargé d'une injection
                if(method.isAnnotationPresent(AutocableSetter.class)) setterInjection(method, serviceInstance);
            }
            //Pour chaque attribut, on vérifie s'il demande une injection avec l'annotation Autocable
            for(Field field : serviceInstance.getClass().getDeclaredFields()){
                fieldInjection(field, serviceInstance);
            }
        }
        //Une fois que toutes les classes ont été ajoutées, instanciées et créés, on instancie les objets ayant
        //des injections par constructeurs
        buildComplexObjects();
    }

    private Object addInstance(Class<?> serviceClass) throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {
        //Si non, on récupère le constructor par défaut et on le met accessible par notre Container
        Constructor<?> constructor = serviceClass.getConstructor();
        constructor.setAccessible(true);
        //On instancie l'objet et on l'ajoute à la liste d'instances
        Object serviceInstance = constructor.newInstance();
        this.serviceInstances.add(serviceInstance);
        return serviceInstance;
    }

    /*
        Méthode réalisant les injections par constructeur et ajoutant les instances à la liste
     */
    private void buildComplexObjects() {
        for(Constructor<?> service : objectsToBuild)
        {
            //Pour chaque objet de la liste
            ArrayList<Object> args = new ArrayList<>();
            //Pour chaque argument à injecter
            for(Class<?> arg : service.getParameterTypes())
            {
                //Obtenir instance depuis ServiceInstances
                args.add(getServiceInstance(arg));
            }
            //On rend disponible le constructeur
            service.setAccessible(true);
            Object serviceInstance = null;
            try {
                //On essaie de l'instancier
                serviceInstance = service.newInstance(args.toArray());
            } catch (InstantiationException | IllegalAccessException |
                    InvocationTargetException | IllegalArgumentException e) {
                e.printStackTrace();
            }
            //On l'ajoute finalement à la liste des instances disponibles
            this.serviceInstances.add(serviceInstance);
        }
    }

    /*
        Fonction retournant une isntance de la classe demandée
     */
    public <T> T getServiceInstance(Class<T> serviceClass){
        for(Object serviceInstance : this.serviceInstances){
            if(serviceClass.isInstance(serviceInstance)){
                try {
                    verifyFields(serviceInstance);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                return (T)serviceInstance;
            }
        }
        return null;
    }

    /*
        Methode vérifiant que les champs d'une classe à injecter sont eux aussi bien injectés
     */
    private void verifyFields(Object serviceInstance) throws IllegalAccessException {
        //Pour chaque champ de l'instance fournie
        for(Field field : serviceInstance.getClass().getDeclaredFields()){
            field.setAccessible(true);
            //Si le champ n'est pas injectable, on passe au suivant
            if(!field.isAnnotationPresent(Autocable.class)) continue;
            //Si le champ est injectable mais est nul
            if(field.get(serviceInstance)==null)
            {
                //Si le champ est marqué par @Valeur, on essaie de refaire l'affectation
                if(field.isAnnotationPresent(Valeur.class))
                {
                    Valeur annotation = field.getAnnotation(Valeur.class);
                    //Recupération directe de valeur
                    //field.set(serviceInstance, annotation.valeur());
                    //Utilisation du registre
                    field.set(serviceInstance,values.getValue(annotation.valeur()));
                }
                //Sinon, on essaie de réinjecter l'instance
                else
                {
                    Class<?> fieldType = field.getType();
                    field.setAccessible(true);
                    for (Object matchPartner : this.serviceInstances) {
                        if (fieldType.isInstance(matchPartner)) field.set(serviceInstance, matchPartner);
                    }
                }
            }
        }
    }

    /*
        Enregistre une nouvelle classe dans le container et met à disposition une instance
     */
    public <T> void register(Class<?> serviceClassImpl) throws Exception
    {
        Object serviceInstance = addInstance(serviceClassImpl);
        for(Method method : serviceInstance.getClass().getMethods())
        {
            //On vérifie si une méthode est un setter chargé d'une injection
            if(method.isAnnotationPresent(AutocableSetter.class)) setterInjection(method, serviceInstance);
        }
        for(Field field : serviceInstance.getClass().getDeclaredFields()){
            fieldInjection(field, serviceInstance);
        }
    }

    /*
        Associe un type abstrait à une implémentation
    */
    public <T> void register(Class<?> serviceClass, Class<?> serviceClassImpl) throws Exception
    {
        this.abstractRegisty.put(serviceClass, serviceClassImpl);
        register(serviceClassImpl);
    }

    /*
        Méthode chargée des injections par attributs et de valeurs
     */
    private void fieldInjection(Field field, Object serviceInstance) throws IllegalAccessException {
        //S'il ne la possède pas, on passe au champ suivant
        if(!field.isAnnotationPresent(Autocable.class)){
            return;
        }
        //S'il possède en plus l'annotation Valeur, on récupère la clé fournie
        if(field.isAnnotationPresent(Valeur.class))
        {
            Valeur annotation = field.getAnnotation(Valeur.class);
            //Recupération directe de valeur
            //field.set(serviceInstance, annotation.valeur());
            //Utilisation du registre
            field.set(serviceInstance,values.getValue(annotation.valeur()));
        }
        else {
            //On réalise l'injection
            Class<?> fieldType = field.getType();
            field.setAccessible(true);
            for (Object matchPartner : this.serviceInstances) {
                //On retrouve l'instance demandée
                if (fieldType.isInstance(matchPartner)) {
                    //On injecte l'objet dans l'attribut
                    field.set(serviceInstance, matchPartner);
                }
            }
        }
    }

    /*
        Méthode réalisant une injection par setter
     */
    private void setterInjection(Method method, Object serviceInstance) {
            //Apel du setter avec une instance du paramètre demandé
            try {
                method.invoke(serviceInstance, getServiceInstance(method.getParameterTypes()[0]));
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
    }

    /*
        Retourne une implémentation concrète d'une interface à partir du registre de classe abstraites
     */
    public <T> Object getAnyServiceInstance(Class<?> abstractClass){
        Class<?> type = abstractRegisty.get(abstractClass);
        return getServiceInstance(type);
    }
}
