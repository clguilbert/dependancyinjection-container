package main.java.DIC.Services.Movies;

/*
    Interface de movie finder, possédant un AuditService, une méthode retournant son type et une méthode utilisant
    l'audit serive pour enregistrer la recherche d'un film
 */
public interface MovieFinder {
    AuditService getAuditService();
    void setAuditService(final AuditService as);

    String getType();
    void log(String movie);
}
