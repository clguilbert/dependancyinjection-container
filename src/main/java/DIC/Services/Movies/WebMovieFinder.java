package main.java.DIC.Services.Movies;

import main.java.DIC.Annotations.Autocable;

/*
    Classe concrète WebMovieFinder, possédant un AuditService injecté par le container après registration,
    une méthode retournant son type WebMovieFinder et une méthode utilisant
    l'audit serive pour enregistrer la recherche d'un film
 */
public class WebMovieFinder implements MovieFinder{
    @Autocable
    AuditService as;

    @Override
    public AuditService getAuditService() {
        return as;
    }

    @Override
    public void setAuditService(AuditService as) {
        this.as = as;
    }

    @Override
    public String getType() {
        return "Web Movie Finder";
    }

    @Override
    public void log(String movie) { as.log("Film " + movie + " trouvé par " + getType()); }
}
