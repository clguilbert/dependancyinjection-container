package main.java.DIC.Services.Movies;

import main.java.DIC.Annotations.Autocable;

/*
    Classe concrète FileMovieFinder, possédant un AuditService injecté par le container après registration,
    une méthode retournant son type FileMovieFinder et une méthode utilisant
    l'audit serive pour enregistrer la recherche d'un film
 */
public class FileMovieFinder implements MovieFinder{

    @Autocable
    AuditService as;

    @Override
    public AuditService getAuditService() {
        return as;
    }

    @Override
    public void setAuditService(AuditService as) {
        this.as = as;
    }

    @Override
    public String getType() {
        return "File Movie Finder";
    }

    @Override
    public void log(String movie) { as.log("Film " + movie + " trouvé par " + getType()); }
}