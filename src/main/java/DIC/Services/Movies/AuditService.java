package main.java.DIC.Services.Movies;

import java.util.ArrayList;

/*
    Classe servant à enregistrer les opérations réalisées dans le package Movies
 */
public class AuditService {
    private ArrayList<String> logs;

    /*
        Initialise la liste de logs à la construction
     */
    public AuditService() { logs = new ArrayList<String>(); }

    /*
        Retourne la dernière opération loggée
     */
    public String getLastLog()
    {
        return logs.get(logs.size() - 1);
    }

    /*
        Enregistre une opération
     */
    public void log(String s)
    {
        logs.add(s);
    }
}
