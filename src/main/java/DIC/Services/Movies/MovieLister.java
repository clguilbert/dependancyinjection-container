package main.java.DIC.Services.Movies;

import main.java.DIC.Annotations.Autocable;
import main.java.DIC.Annotations.Scannable;

import java.util.ArrayList;

/*
    Classe exemple du sujet, possédant une liste de films et un movie finder servant à rechercher des films à ajouter
    et à logger ses recherches grâce à un auditService.
 */
@Scannable
public class MovieLister {

    private ArrayList<String> movieList;

    /*
        MovieFinder, injecté et spécialisé par le container
     */
    @Autocable
    private MovieFinder mf;

    /*
        Methode servant à ajouter un film grâce au movieFinder
        Initialise la liste si elle ne l'est pas déjà
     */
    public void AddMovie(String movie)
    {
        mf.log(movie);
        if(movieList == null) movieList = new ArrayList<>();
        movieList.add(movie);
    }

    /*
        Methode retournant la liste de films
     */
    public ArrayList<String> getMovies()
    {
        return movieList;
    }

    /*
        Methode retournant la dernière opération enregistrée par l'auditService du movieFinder
     */
    public String getLastLog() { return mf.getAuditService().getLastLog(); }
}
