package main.java.DIC.Services.Garage;

import main.java.DIC.Annotations.AutocableConstructor;
import main.java.DIC.Annotations.Scannable;

/*
    Classe Voiture possédant un moteur de ville et une radio tranquille, injectés par le constructeur
    Une Voiture ne peut fonctionner que si ses attributs sont au bon type
 */
@Scannable
public class Voiture implements Vehicule {

    private MoteurVille moteur;
    private RadioTranquille radio;

    /*
        Constructeur de la voiture, prenant des classes spécialisées
    */
    @AutocableConstructor
    public Voiture(MoteurVille m, RadioTranquille r)
    {
        moteur = m;
        radio =  r;
    }

    /*
        Setter du moteur, castant un moteur en MoteurVille
     */
    @Override
    public void setMoteur(Moteur m) {
        this.moteur = (MoteurVille) m;
    }

    /*
        Setter de la radio, castant une radio en RadioTranquille
    */
    @Override
    public void setRadio(Radio r) {
        this.radio = (RadioTranquille) r;
    }

    /*
        methode retournant une chaine de caractère décrivant une Voiture
     */
    @Override
    public String sortirDuGarage() {
        return ("La voiture normale sort du garage, " + moteur.demarrer() + radio.allumer());
    }
}
