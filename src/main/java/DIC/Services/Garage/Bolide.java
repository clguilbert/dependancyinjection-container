package main.java.DIC.Services.Garage;

import main.java.DIC.Annotations.AutocableConstructor;
import main.java.DIC.Annotations.Scannable;

/*
    Classe Bolide possédant un moteur de sport et un caisson de basse, injectés par le constructeur
    Un Bolide ne fonctionne pas si ses attributs ne sont pas au bon type
 */

@Scannable
public class Bolide implements Vehicule {

    private MoteurSport moteur;
    private CaissonBasses radio;

    /*
        Constructeur du bolide, prenant des classes spécialisées
     */
    @AutocableConstructor
    public Bolide(MoteurSport m, CaissonBasses r)
    {
        moteur = m;
        radio = r;
    }

    /*
        Setter du moteur, castant un moteur en MoteurSport
     */
    @Override
    public void setMoteur(Moteur m) {
        this.moteur = (MoteurSport) m;
    }

    /*
        Setter de la radio, castant une radio en CaissonBasses
    */
    @Override
    public void setRadio(Radio r) {
        this.radio = (CaissonBasses) r;
    }

    /*
        methode retournant une chaine de caractère décrivant un Bolide
     */
    @Override
    public String sortirDuGarage() {
        return("Le bolide sort du garage, " + moteur.demarrer() + radio.allumer());
    }
}
