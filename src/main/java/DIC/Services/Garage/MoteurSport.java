package main.java.DIC.Services.Garage;

import main.java.DIC.Annotations.Scannable;

@Scannable
public class MoteurSport implements Moteur {
    @Override
    public String demarrer() {
        return "vroum vroum le V8";
    }
}
