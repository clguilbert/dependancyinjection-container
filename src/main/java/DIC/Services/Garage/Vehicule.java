package main.java.DIC.Services.Garage;

/*
    Interface type des véhicules du package garage. Les méthodes permettent de modifier le moteur et la radio des vehicules
    ainsi que de retourner une chaine de caractère décrivant ces véhicules.
 */
public interface Vehicule {
    void setMoteur(final Moteur m);
    void setRadio(final Radio r);
    String sortirDuGarage();
}
