package main.java.DIC.Services.Garage;

import main.java.DIC.Annotations.AutocableConstructor;
import main.java.DIC.Annotations.Scannable;

/*
    Classe Hybride possédant un moteur et une radio, injectés par le constructeur
    Une Hybride peut fonctionner quelque soit le type concret de ses attributs
 */
@Scannable
public class Hybride implements Vehicule {

    private Moteur moteur;
    private Radio radio;

    /*
        Constructeur de l'hybride, prenant par defaut des classes spécialisées
     */
    @AutocableConstructor
    public Hybride(MoteurVille m, CaissonBasses r)
    {
        moteur = m;
        radio = r;
    }

    /*
        Setter du moteur, ne proposant pas de cast
     */
    @Override
    public void setMoteur(Moteur m) {
        moteur = m;
    }

    /*
        Setter de la radio, ne proposant pas de cast
    */
    @Override
    public void setRadio(Radio r) {
        radio = r;
    }

    /*
        methode retournant une chaine de caractère décrivant une hybride
    */
    @Override
    public String sortirDuGarage() {
        return("L'hybride tunée sort du garage, " + moteur.demarrer() + radio.allumer());
    }
}
