package main.java.DIC.Services.Registerable;

/*
    Classe modélisant une chaussette courte, de taille 10
 */
public class ChaussetteCourte implements Chaussette {
    @Override
    public int getTaille() {
        return 10;
    }
}
