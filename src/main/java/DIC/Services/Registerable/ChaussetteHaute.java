package main.java.DIC.Services.Registerable;

/*
    Classe modélisant une chaussette longue, de taille 20
 */
public class ChaussetteHaute implements Chaussette {
    @Override
    public int getTaille() {
        return 20;
    }
}
