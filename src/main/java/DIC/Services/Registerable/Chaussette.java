package main.java.DIC.Services.Registerable;

/*
    Interface d'une chaussette, possédant une méthode retournant la taille de la chaussette piochée dans le tiroir
    Sert à présenter l'injection d'une chaussette d'une taille concrète à la demande d'une chaussette au container
 */
public interface Chaussette {
    int getTaille();
}
