package main.java.DIC.Services.GraphResolution;

import main.java.DIC.Annotations.Autocable;
import main.java.DIC.Annotations.Scannable;

/*
    Classe utilisée par A possédant un D affecté par le container et une méthode décrivant son D
 */
@Scannable
public class C {
    @Autocable
    protected D d;

    public String bonjour()
    {
        return "que le D de C dit " + d.bonjour();
    }
}
