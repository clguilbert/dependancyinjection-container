package main.java.DIC.Services.GraphResolution;

import main.java.DIC.Annotations.Autocable;
import main.java.DIC.Annotations.Scannable;

/*
    Classe au sommet du graphe de dépendance, possédant une méthode servant à décrire ses enfants et une méthode
    faite pour être appelée par une de ses dépendances enfants. Le graphe de dépendance est résolu en injectant les
    dépendances par attributs.
 */
@Scannable
public class A {
    /*
        Dépendances filles injectées par le container
     */
    @Autocable
    private B b;
    @Autocable
    private C c;

    public String bonjour()
    {
        return("Bonjour \n Le B de A dit : " + b.bonjour() + "\n Le C de A dit : " + c.bonjour());
    }

    public String oui() {
        return("une dépendance m'a appelé");
    }
}
