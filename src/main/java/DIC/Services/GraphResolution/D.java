package main.java.DIC.Services.GraphResolution;

import main.java.DIC.Annotations.Autocable;
import main.java.DIC.Annotations.Scannable;

/*
    Classe utilisée par B et C possédant un E et un A affecté par le container.
    Possède une méthode appelant son A et une méthode décrivant son E
 */
@Scannable
public class D {
    @Autocable
    public E e;

    @Autocable
    private A a;

    public String bonjour()
    {
        return "le E de D dit " + e.bonjour();
    }

    public String aEstValide()
    {
        return a.oui();
    }
}
