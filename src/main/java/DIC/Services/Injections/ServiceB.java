package main.java.DIC.Services.Injections;

/*
    Interface décrivant les méthodes du ServiceA, un service possédant un ServiceA.
 */
public interface ServiceB {
    ServiceA getServiceA();

    void setServiceA(final ServiceA serviceA);

    String jobB();
}
