package main.java.DIC.Services.Injections;

import main.java.DIC.Annotations.Autocable;
import main.java.DIC.Annotations.Scannable;

/*
    Classe concrète de ServiceB, possédant un serviceA injecté par attribut
 */
@Scannable
public class ServiceBImpl implements  ServiceB{

    @Autocable
    private ServiceA serviceA;

    @Override
    public ServiceA getServiceA() {
        return serviceA;
    }

    @Override
    public void setServiceA(final ServiceA serviceA) {
        this.serviceA = serviceA;
    }

    @Override
    public String jobB() {
        return "jobB()";
    }

}
