package main.java.DIC.Services.Injections;

/*
    Interface décrivant les méthodes du ServiceMaster, un service possédant un ServiceA et un serviceB.
    Possède également des objets affectés par le container à partir du registre de BindingValues
    Les services A et B sont injectés par setter.
    Des méthodes servent à afficher les objets, une pour les services, trois pour les injections par valeur
 */
public interface ServiceMaster {
    ServiceA getServiceA();
    ServiceB getServiceB();

    Object getTextInjecte();
    Object getTextInjecte2();
    Object getTextInjecte3();

    void setServiceA(final ServiceA serviceA);
    void setServiceB(final ServiceB serviceB);

    String jobPrint();
}
