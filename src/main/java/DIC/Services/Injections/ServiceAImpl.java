package main.java.DIC.Services.Injections;

import main.java.DIC.Annotations.Autocable;
import main.java.DIC.Annotations.Scannable;

/*
    Classe concrète de ServiceA, possédant un serviceB injecté par attribut et utilisé dans sa méthode jobA()
 */
@Scannable
public class ServiceAImpl implements  ServiceA{

    @Autocable
    private ServiceB serviceB;

    @Override
    public ServiceB getServiceB() {
        return serviceB;
    }

    @Override
    public void setServiceB(final ServiceB serviceB) {
        this.serviceB = serviceB;
    }

    /*
        Méthode affichant un message dépendant du jobB
     */
    @Override
    public String jobA() {
        return "jobA(" + this.serviceB.jobB() + ")";
    }

}
