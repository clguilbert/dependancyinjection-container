package main.java.DIC.Services.Injections;

/*
    Interface décrivant les méthodes du ServiceA, un service possédant un ServiceB.
 */
public interface ServiceA {
    ServiceB getServiceB();

    void setServiceB(final ServiceB serviceB);

    String jobA();
}
