package main.java.DIC.Services.Injections;

import main.java.DIC.Annotations.*;

/*
    Classe concrète de ServiceMaster, possédant un serviceA et un serviceB injectés par setter
 */
@Scannable
public class ServiceMasterImpl implements ServiceMaster {

    private ServiceA serviceA;
    private ServiceB serviceB;

    /*
        Injecte un objet ayant pour clé "int" => 42 dans le registre
     */
    @Autocable
    @Valeur(valeur = "int")
    public Object injecterTexteIci;

    /*
        Injecte un objet ayant pour clé "oups" => bonjour dans le registre
     */
    @Autocable
    @Valeur(valeur = "oups")
    public Object injecterTexteIci2;

    /*
        Injecte un objet ayant pour clé "rien" => n'existe pas dans le registre
    */
    @Autocable
    @Valeur(valeur = "rien")
    public Object injecterTexteIci3;

    /*
        Getter de champs Objects
     */
    @Override
    public Object getTextInjecte(){return injecterTexteIci;}
    @Override
    public Object getTextInjecte2(){return injecterTexteIci2; }
    @Override
    public Object getTextInjecte3(){return injecterTexteIci3;}

    /*
        Getter de champs Service
    */
    @Override
    public ServiceA getServiceA() {return serviceA;}
    @Override
    public ServiceB getServiceB() {return serviceB;}

    /*
        Setters servant à l'injection de services
     */
    @Override
    @AutocableSetter
    public void setServiceA(final ServiceA serviceA) { this.serviceA = serviceA; }
    @Override
    @AutocableSetter
    public void setServiceB(final ServiceB serviceB) { this.serviceB = serviceB; }

    /*
        Methode affichant un message dépendant des deux services injectés
     */
    @Override
    public String jobPrint() { return "(" + getServiceA().jobA() + " " + getServiceB().jobB() + ")"; }
}
