package main.java.DIC;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;

/*
    Classe chargée de retrouver toutes les classes marquées par @Scannable et de les ajouter au registre
 */
public class DIClassScanner {

    private String rootPackage;

    /*
        Constructeur du ClassScanner lui fournissant un package d'origine
     */
    DIClassScanner(String rootPackage){this.rootPackage = rootPackage;}

    /*
        Renvoie une nouvelle instance de ClassScanner avec le rootPackage fourni
     */
    public static DIClassScanner getInstance(String rootPackage) {
        return new DIClassScanner(rootPackage);
    }

    /*
        Renvoie une liste d'adresses des fichiers trouvés dans le classLoader du JRE
     */
    private Enumeration<URL> listFilesInClassLoader() throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return classLoader.getResources("");
    }

    /*
        Methode retrouvant toutes les classes marquées d'un certain type d'annotation, ici Scannable
     */
    public <T> Collection<Class<T>> findAllClassHavingAnnotation(Class<T> annotation) throws ClassNotFoundException, IOException {
        Collection<Class<T>> returned = new LinkedList<>();
        try {
            Enumeration<URL> urls = listFilesInClassLoader();
            //Pour chaque fichier trouvé
            while(urls.hasMoreElements()) {
                URL next = urls.nextElement();
                File curDir = new File(next.getFile());
                //Pour chaque classe trouvée, vérifie si l'annotation est présente
                for (Class curClass : this.findClassesInPackage(curDir)) {
                    if (curClass.isAnnotationPresent(annotation)) {
                        //Si oui, l'ajoute à la liste de classes à retourner
                        returned.add(curClass);
                    }
                }
            }
        } catch (IOException e) {
            throw new IOException("Erreur : ressource non trouvée par le classLoader");
        }
        return returned;
    }

    /*
        Methode servant à extraire les classes d'un package
     */
    private <T> Collection<Class<?>> findClassesInPackage(File curDir) throws ClassNotFoundException {
        Collection<Class<?>> classes = new LinkedList<>();
        if (curDir.exists()) {
            File[] content = curDir.listFiles();
            //Pour chaque fichier du package
            for (File curFile : content) {
                //S'il existe un sous package, on le parcours récursivement
                if (curFile.isDirectory()) {
                    classes.addAll(this.findClassesInPackage(curFile));
                } else {
                    try {
                        //On éjecte les sous packages qui ne sont pas dans notre zone de travail
                        if (!curFile.getPath().contains("DIC") || curFile.getPath().contains("test")) break;
                        String path = curFile.getAbsolutePath();
                        String asPackageFormat = path.replaceAll("\\\\", ".");
                        int indexOfPackageRoot = asPackageFormat.indexOf(this.rootPackage);
                        //On récupère le nom de la classe à partir de l'adresse
                        String fullClassName = asPackageFormat.substring(indexOfPackageRoot, asPackageFormat.length() - 6);
                        //On l'ajoute aux classes à vérifier
                        classes.add(Class.forName(fullClassName));
                    } catch (ClassNotFoundException e) {
                        throw new ClassNotFoundException("Une classe n'a pas été trouvée");
                    }
                }
            }
        }
        return classes;
    }
}
