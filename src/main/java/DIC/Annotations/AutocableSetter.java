package main.java.DIC.Annotations;

import java.lang.annotation.*;

/*
 * Annotation servant à identifier les setters qui seront chargés d'injecter des dépendances
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AutocableSetter {
}
