package main.java.DIC.Annotations;

import java.lang.annotation.*;

/*
 * Annotation servant à identifier les constructeurs chargés d'injecter des dépendances
 */

@Target(ElementType.CONSTRUCTOR)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AutocableConstructor {
}
