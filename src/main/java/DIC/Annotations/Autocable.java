package main.java.DIC.Annotations;

import java.lang.annotation.*;

/*
 * Annotation remplacant Autowired servant à identifier les champs dans lesquels injecter des dépendances
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Autocable {

}
