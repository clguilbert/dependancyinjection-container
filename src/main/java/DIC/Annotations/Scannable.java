package main.java.DIC.Annotations;

import java.lang.annotation.*;

/*
 * Annotation servant à identifier les classes qui seront repérées et chargées dans le contexte par le classScanner
 */

@Target(ElementType.TYPE)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface Scannable {
}
