package test.java.DIC;

import org.junit.jupiter.api.Test;

class DICApplicationTests {

	@Test
	void contextLoads() {
		ContainerTests cT = new ContainerTests();
		cT.TestContainer();

		GarageTests gT = new GarageTests();
		gT.TestGarage();

		GraphResolutionTests gRT = new GraphResolutionTests();
		gRT.TestGraphResolution();

		MoviesTests mT = new MoviesTests();
		mT.TestMovies();

		ServicesTests sT = new ServicesTests();
		sT.TestServices();
	}
}
