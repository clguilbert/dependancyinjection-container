package test.java.DIC;

import main.java.DIC.Services.Garage.*;
import org.junit.jupiter.api.Test;
import main.java.DIC.DIClassScanner;
import main.java.DIC.DIContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GarageTests {
    @Test
    public void TestGarage() {
        System.out.println("Test du package Garage, illustrant l'injection par constructeur");
        DIContext context = null;
        try {
            context = DIContext.createContext();
            assertNotNull(context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bolide b = context.getServiceInstance(Bolide.class);
        assertNotNull(b);
        Hybride h = context.getServiceInstance(Hybride.class);
        assertNotNull(h);
        Voiture v = context.getServiceInstance(Voiture.class);
        assertNotNull(v);

        assertEquals("Le bolide sort du garage, vroum vroum le V8 Boum boum dans les oreilles", b.sortirDuGarage());
        assertEquals("L'hybride tunée sort du garage, pas trop vite c'est limité à 50 Boum boum dans les oreilles",h.sortirDuGarage());
        assertEquals("La voiture normale sort du garage, pas trop vite c'est limité à 50 pas trop fort la musique hein",v.sortirDuGarage());

        h.setMoteur(context.getServiceInstance(MoteurSport.class));
        h.setRadio(context.getServiceInstance(RadioTranquille.class));
        assertEquals("L'hybride tunée sort du garage, vroum vroum le V8 pas trop fort la musique hein",h.sortirDuGarage());
        System.out.println("Fin des tests du garage");
    }
}
