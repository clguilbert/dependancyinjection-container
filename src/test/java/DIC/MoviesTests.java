package test.java.DIC;

import main.java.DIC.DIContext;
import main.java.DIC.Services.Movies.AuditService;
import main.java.DIC.Services.Movies.MovieFinder;
import main.java.DIC.Services.Movies.MovieLister;
import main.java.DIC.Services.Movies.WebMovieFinder;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MoviesTests {
    @Test
    public void TestMovies() {
        System.out.println("Test de l'example du sujet utilisant le package Movies");
        DIContext context = null;
        try {
            context = DIContext.createContext();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            context.register(AuditService.class);
            context.register(MovieFinder.class, WebMovieFinder.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        MovieLister movieLister = context.getServiceInstance(MovieLister.class);
        assertNotNull(movieLister);
        movieLister.AddMovie("La soupe aux Choux");
        List<String> films = movieLister.getMovies();
        assertNotNull(films);
        assertEquals("La soupe aux Choux", films.get(0));
        assertEquals("Film " + films.get(0) + " trouvé par Web Movie Finder", movieLister.getLastLog());
        System.out.println("Fin des tests portant sur les films");
    }
}
