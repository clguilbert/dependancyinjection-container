package test.java.DIC;

import main.java.DIC.DIContext;
import main.java.DIC.Services.GraphResolution.A;
import main.java.DIC.Services.GraphResolution.D;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GraphResolutionTests {
    @Test
    public void TestGraphResolution() {
        DIContext context = null;
        try {
            context = DIContext.createContext();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Test de l'injection par attribut sur un graphe de dépendances");
        System.out.println("Description du Graphe : A utilise un B et un C, B et C utilisent un D, D utilise un E et possède un A");
        A a = context.getServiceInstance(A.class);
        System.out.println(a.bonjour());
        assertNotNull(a);
        assertEquals("Bonjour \n" +
                " Le B de A dit : que le D de B dit le E de D dit bonjour!\n" +
                " Le C de A dit : que le D de C dit le E de D dit bonjour!",a.bonjour());
        D d = context.getServiceInstance(D.class);
        System.out.println(d.aEstValide());
        assertNotNull(d);
        assertEquals("une dépendance m'a appelé",d.aEstValide());
        System.out.println("Fin des tests sur le graphe de dépendance");
    }
}
