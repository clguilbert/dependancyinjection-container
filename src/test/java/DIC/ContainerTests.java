package test.java.DIC;

import main.java.DIC.Annotations.Scannable;
import main.java.DIC.Annotations.Valeur;
import main.java.DIC.DIClassScanner;
import main.java.DIC.DIContext;
import main.java.DIC.Services.GraphResolution.A;
import main.java.DIC.Services.Registerable.Chaussette;
import main.java.DIC.Services.Registerable.ChaussetteCourte;
import main.java.DIC.Services.Registerable.ChaussetteHaute;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;


public class ContainerTests {
    @Test
    public void TestContainer()
    {
        try{
            System.out.println("Test de création de contexte");
            DIContext context = DIContext.createContext();
            assertNotNull(context);
            System.out.println("Test de scan avec mauvaise recherche d'annotation");
            DIClassScanner.getInstance("").findAllClassHavingAnnotation(DIContext.class);
        }
        catch (ClassNotFoundException e){
            assertEquals("Une classe n'a pas été trouvée", e.getMessage());
        } catch (IOException e) {
            assertEquals("Erreur : ressource non trouvée par le classLoader", e.getMessage());
        } catch (Exception e) {
            assertEquals( "Erreur lors de la création du contexte", e.getMessage());
        }

        try{
            DIContext context = DIContext.createContext();
            assertNotNull(context);

            System.out.println("Test de récupération d'instance composée et d'appel de ses méthodes");
            A a = context.getServiceInstance(A.class);
            assertNotNull(a);
            assertEquals(a.bonjour(), "Bonjour \n" +
                    " Le B de A dit : que le D de B dit le E de D dit bonjour!\n" +
                    " Le C de A dit : que le D de C dit le E de D dit bonjour!");

            System.out.println("Test de récupération d'instance non enregistrée");
            Valeur v = context.getServiceInstance(Valeur.class);
            assertNull(v);

            System.out.println("Test d'enregistrement de classe & binding Object");
            context.register(ChaussetteCourte.class);
            ChaussetteCourte cc = context.getServiceInstance(ChaussetteCourte.class);
            assertNotNull(cc);
            assertEquals(cc.getTaille(),10);

            System.out.println("Test d'enregistrement de classe abstraite");
            context.register(Chaussette.class, ChaussetteHaute.class);

            System.out.println("Test de récupération de classe concrète à partir de classe abstraite");
            ChaussetteHaute ch = (ChaussetteHaute) context.getAnyServiceInstance(Chaussette.class);
            assertNotNull(ch);
            assertEquals(ch.getTaille(), 20);

            System.out.println("Fin des tests de container");
        }
        catch (ClassNotFoundException e){
            assertEquals( "Une classe n'a pas été trouvée", e.getMessage());
        } catch (IOException e) {
            assertEquals( "Erreur : ressource non trouvée par le classLoader", e.getMessage());
        } catch (Exception e) {
            assertEquals("Erreur lors de la création du contexte", e.getMessage());
        }
    }
}
