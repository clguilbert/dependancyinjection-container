package test.java.DIC;


import main.java.DIC.DIContext;
import main.java.DIC.Services.Injections.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ServicesTests {
    @Test
    public void TestServices() {
        System.out.println("Test des services basiques, du binding value et de l'injection par setter");
        DIContext context = null;
        try {
            context = DIContext.createContext();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ServiceMaster serviceM = context.getServiceInstance(ServiceMasterImpl.class);
        assertNotNull(serviceM);
        assertEquals("(jobA(jobB()) jobB())",serviceM.jobPrint());

        System.out.println("Tests du binding value");
        assertEquals(42,serviceM.getTextInjecte());
        assertEquals("bonjour",serviceM.getTextInjecte2());
        assertNull(serviceM.getTextInjecte3());

        System.out.println("Tests de l'injection par Setter");
        assertEquals(serviceM.getServiceA().getServiceB().jobB(),serviceM.getServiceB().jobB());
        System.out.println("Fin des tests de service");
    }
}
